# My dotfiles and more

Includes:
* Scripts (`bin`)
* Configs (`config`)
* Few docs with how-to's (`docs`)
* OS setup
  * Gentoo (`gentoo`)
    * Thinkpad T430 

Using NixOS? Check [here](https://codeberg.org/wolfangaukang/nix-agordoj)!
