# Setting up Gentoo with dmcrypt and SELinux

## Guides used:
- [Gentoo Handbook, Installation section](https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation)
- [Full Disk Encryption From Scratch, Simplified](https://wiki.gentoo.org/wiki/Full_Disk_Encryption_From_Scratch_Simplified)
- [SELinux Installation](https://wiki.gentoo.org/wiki/SELinux/Installation)
- [Sakaki's Guide, network section (Connect to wireless)](https://wiki.gentoo.org/wiki/User:Sakaki/Sakaki%27s_EFI_Install_Guide/Setting_Up_Networking_and_Connecting_via_ssh#Connecting_via_Wired_Ethernet)
- As optional
  - [Ryzen setup](https://wiki.gentoo.org/wiki/Ryzen)
