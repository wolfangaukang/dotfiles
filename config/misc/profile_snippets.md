# Profile snippets

Useful to have this as a reference when setting up something

## Source .bashrc if it exists and if running Bash

```
if [ -n "$BASH_VERSION" ]; then
	if [ -f "$HOME/.bashrc" ]; then . "$HOME/.bashrc"; fi
fi
```

## Export .local/bin to path

``
if [ -d "$HOME/.local/bin" ]; then PATH="$HOME/.local/bin:$PATH"; fi
``

## Run Sway (PROBABLY OBSOLETE)

**NOTE:** Consider the following:
- `$DISPLAY` does not exist
- `You are using VTNR 1, which means the terminal 1 
  - `$XDG_VTNR` is `systemd` exclusive.
  - And not even reliable, according to [this](https://unix.stackexchange.com/questions/521037/what-is-the-environment-variable-xdg-vtnr).
- To determine the layout and options, `$XKB_DEFAULT_LAYOUT`, `$XKB_DEFAULT_VARIANT` and `$XKB_DEFAULT_OPTIONS` are used.
```
# In case swaysock is necessary
export SWAYSOCK=$(ls /run/user/$(id -u)/sway-ipc.* | head -n 1)
# In case LC_CTYPE is necessary
export LC_CTYPE="en_US.utf8"
if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
  export XKB_DEFAULT_LAYOUT=us,es
  export XKB_DEFAULT_VARIANT=colemak # This for only us. Might not work with es.
  export XKB_DEFAULT_OPTIONS=grp:alt_space_toggle,
  exec sway -d 2> sway.log
fi
```

Another way to run it, that has been tested but might require improvements, is:

```
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
  if [ -x "$(command -v sway)" ]; then exec sway; fi
fi
```

## Run i3 (PROBABLY OBSOLETE)

Check above, but instead of using `exec sway`, use `exec startx`. Also, consider the ~/.xinitrc file should have the following:

```
if [ -d /etc/X11/xinit/xinitrc.d ]; then
  for f in /etc/X11/xinit/xinitrc.d/*; do
    [ -x "$f" ] && . "$f"
  done
  unset f
fi
# Set Colemak as layout
setxkbmap us -variant colemak
# Allow key repetition for Caps Lock
xset r 66
# Setting keycodes
xmodmap ~/.Xmodmap
# Setting brightness
echo 1300 > /sys/class/backlight/intel_backlight/brightness
# Setting Xresources for urxvt, xft and rofi
if [ -f ~/.Xresources ]; then xrdb -merge ~/.Xresources; fi

exec i3 --shmlog-size=26214400
```

## Run tmux each time a session is opened

```
if [ -x "$(command -v tmux)" ] && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
  exec tmux
fi
```
