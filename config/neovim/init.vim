set number
" https://jdhao.github.io/2019/01/11/line_number_setting_nvim/
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter *   set relativenumber!
augroup END
