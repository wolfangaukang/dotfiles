-- TODO: Create functions to handle this
-- TODO: Also, see how to separate these files
local lspconfig = require('lspconfig')
lspconfig.bashls.setup({})
lspconfig.dockerls.setup({})
lspconfig.pyright.setup({})
lspconfig.rnix.setup({})
lspconfig.lua_ls.setup({})
lspconfig.terraformls.setup({})
lspconfig.yamlls.setup({
  redhat = {
    telemetry = {
      enabled = false
    }
  },
  settings = {
    yaml = {
      -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#yamlls
      schemas = {
        [ "https://raw.githubusercontent.com/yannh/kubernetes-json-schema/master/v1.18.0-standalone-strict/all.json" ] = "/*.k8s.yaml",
      },
      -- CloudFormation
      customTags = {
        "!FindInMap",
        "!FindInMap sequence",
        "!GetAtt",
        "!GetAtt sequence",
        "!Join",
        "!Join sequence",
        "!Ref",
        "!Ref sequence",
        "!Select",
        "!Select sequence",
        "!Split",
        "!Split sequence",
        "!Sub",
        "!Sub sequence",
      }
    }
  }
})
