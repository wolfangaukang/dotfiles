local spectre = require('spectre')
spectre.setup()
vim.cmd "command! Spectre lua spectre.open()"
-- TODO: Test these
-- vim.cmd "command! SpectreVisual lua spectre.open_visual()"
-- vim.cmd "command! SpectreCurrentFile lua spectre.open_file_search()"
