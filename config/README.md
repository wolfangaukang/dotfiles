# Configuration files

## Applications
- [Alacritty](https://github.com/alacritty/alacritty): A cross-platform, OpenGL terminal emulator. 
- [Bashblog](https://github.com/cfenollosa/bashblog): A single Bash script to create blogs. Download, run, write, done!
- [GTK](https://gitlab.gnome.org/GNOME/gtk): Multi-platform toolkit for creating graphical user interfaces.
- [Ranger](https://github.com/ranger/ranger): A VIM-inspired filemanager for the console.
- [RXVT-Unicode](https://github.com/exg/rxvt-unicode): Fast and lightweight terminal emulator with Xft and Unicode support.

## Package Managers
- [Nix](https://github.com/NixOS/nix): Purely functional package manager 
  - For more stuff regarding NixOS, nixpkgs and home-manager, check `nix` at the root 

## Shell Management
- Profile: User-wide variables and settings.
- [Zsh](https://github.com/zsh-users/zsh): Extended Bourne shell with many improvements, including some features of Bash, ksh, and tcsh.

## Wayland related
- [kanshi](https://github.com/emersion/kanshi): Dynamic display configuration.
- [Kitty](https://github.com/kovidgoyal/kitty): Cross-platform, fast, feature-rich, GPU based terminal.
- [Sway](https://github.com/swaywm/sway): i3-compatible Wayland compositor
- [Waybar](https://github.com/Alexays/Waybar): Highly customizable Wayland bar for Sway and Wlroots based compositors.
- [Wayfire](https://github.com/WayfireWM/wayfire): A modular and extensible wayland compositor.

## X11-Related
- [Compton](https://github.com/chjj/compton): Compositor for X11. Used for shadowing and transparency.
- [Conky](https://github.com/brndnmtthws/conky): Lightweight system monitor for X.
- [Dunst](https://github.com/dunst-project/dunst): Notification daemon. Provides minimalist notifications.
- [i3](https://github.com/i3/i3): Tiling window manager. There is another version called [i3-gaps](https://github.com/Airblader/i3).
- [i3blocks](https://github.com/vivien/i3blocks): A feed generator for text based status bars.
  - Also contains some modules.
- [i3status](https://github.com/i3/i3status): Generates status bar to use with i3bar, dzen2 or xmobar.
- [Rofi](https://github.com/davatorium/rofi): Rofi: A window switcher, application launcher and dmenu replacement.
- Xkbmap: X Keyboard mapping setup (it works with Sway, even if Wayland related).

## Misc

- misc: Contains references. Not intended for usage.
