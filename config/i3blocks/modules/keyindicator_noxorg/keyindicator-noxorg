#!/usr/bin/env python

from argparse import ArgumentParser
from sys import argv
from evdev import InputDevice, ecodes

DEFAULT_COLOR_OFF = "#222222" 
DEFAULT_COLOR_ON =  "#00FF00"
KEYBOARD_EVENT_PATH = "/dev/input/event"

#Returns a string with the input path
def keyboard_definition(event_number):
   return ''.join([KEYBOARD_EVENT_PATH,str(event_number)])

#Return the input device according to the keyboard definition
def set_input_device(keyboard_path):
   return InputDevice(keyboard_path)

#Return the dictionary of leds available
def set_leds_dictionary(input_device):
   full_led_list = input_device.capabilities(verbose=True)[('EV_LED', ecodes.EV_LED)]
   return dict(full_led_list)

#Returns list of active leds on input device
def get_active_leds(input_device):
   active_leds = input_device.leds()
   return active_leds

def get_keystate(leds_dictionary, instance, active_leds, on_color, off_color):
   uppercaseInstance = instance.upper()
   if leds_dictionary[''.join(['LED_',uppercaseInstance,'L'])] in active_leds:
      return ''.join([uppercaseInstance,"\n",uppercaseInstance,"\n",on_color])
   else:
      return ''.join([uppercaseInstance,"\n",uppercaseInstance,"\n",off_color])

def output_keystate(custom_path, event_number, block_instance, on_color, off_color):
   try:
     if (custom_path is None):
       key_path = keyboard_definition(event_number)
       input_device = set_input_device(key_path)
     else:
       input_device = set_input_device(custom_path)
     leds_dic = set_leds_dictionary(input_device)
     active_leds = get_active_leds(input_device)
     print(get_keystate(leds_dic, block_instance, active_leds, on_color, off_color))
   except KeyError:
     print("The input device does not correspond to a keyboard")
   except FileNotFoundError:
     print("The event number isn't correct or does not exist")
   except KeyError:
     print("The input path isn't valid")

if __name__=="__main__":
   parser = ArgumentParser("Python script to get the keyboard status")
   parser.add_argument("-e", "--event-number", metavar="NUMBER", type=int, help="Set the input device number", dest="EVENT_NUMBER", default=0)
   parser.add_argument("-f", "--off-color", metavar="#XXXXXX", type=str, help="Set the color when key is off", dest="OFF_COLOR", default=DEFAULT_COLOR_OFF)
   parser.add_argument("-n", "--on-color", metavar="#XXXXXX", type=str, help="Set the color when key is on", dest="ON_COLOR", default=DEFAULT_COLOR_ON)
   parser.add_argument("-b", "--block-instance", metavar="CAPS|NUM|SCROLL", type=str, help="Choose the key to check", dest="BLOCK_INSTANCE", choices=["NUM","CAPS","SCROLL"], default="CAPS")
   parser.add_argument("-p", "--input-path", metavar="PATH", type=str, help="Custom path for input device (Default is "+KEYBOARD_EVENT_PATH+"X)", dest="CUSTOM_PATH", default=None)
   args = parser.parse_args()
   output_keystate(args.CUSTOM_PATH, args.EVENT_NUMBER, args.BLOCK_INSTANCE, args.ON_COLOR, args.OFF_COLOR)
   
