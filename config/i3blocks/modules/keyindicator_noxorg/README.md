# keyindicator-noxorg

Show the status of capslock or numlock without depending of Xorg tools (useful for Sway).

# Installation

Add the following bindings to i3 config file:

```
bindsym --release Caps_Lock exec pkill -SIGRTMIN+11 i3blocks
bindsym --release Num_Lock  exec pkill -SIGRTMIN+11 i3blocks
```

Use the following in your i3blocks config file:

``` ini
[keyindicator-noxorg]
command=$SCRIPT_DIR/keyindicator-noxorg
instance=CAPS|NUM|SCROLL
markup=pango
interval=once
signal=11
```

# Options

```
usage: Python script to get the keyboard status [-h] [-e NUMBER] [-f #XXXXXX]
                                                [-n #XXXXXX]
                                                [-b CAPS|NUM|SCROLL] [-p PATH]

optional arguments:
  -h, --help            show this help message and exit
  -e NUMBER, --event-number NUMBER
                        Set the input device number
  -f #XXXXXX, --off-color #XXXXXX
                        Set the color when key is off
  -n #XXXXXX, --on-color #XXXXXX
                        Set the color when key is on
  -b CAPS|NUM|SCROLL, --block-instance CAPS|NUM|SCROLL
                        Choose the key to check
  -p PATH, --input-path PATH
                        Custom path for input device (Default is
                        /dev/input/eventX)
```
