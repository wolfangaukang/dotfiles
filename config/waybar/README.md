# Waybar themes

## Ragana

![Ragana Waybar theme](../../assets/ragana.png)

## Rbnis

![Rbnis Waybar theme](../../assets/rbnis.png)
