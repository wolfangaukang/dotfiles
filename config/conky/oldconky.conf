-- vim: ts=4 sw=4 noet ai cindent syntax=lua
--[[
Conky, a system monitor, based on torsmo

Any original torsmo code is licensed under the BSD license

All code written since the fork of torsmo is licensed under the GPL

Please see COPYING for details

Copyright (c) 2004, Hannu Saransaari and Lauri Hakkarainen
Copyright (c) 2005-2012 Brenden Matthews, Philip Kovacs, et. al. (see AUTHORS)
All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

conky.config = {
    alignment = 'top_right',
    background = false,
    border_width = 1,
    cpu_avg_samples = 2,
        default_color = 'white',
    default_outline_color = 'white',
    default_shade_color = 'white',
    draw_borders = false,
    draw_graph_borders = false,
    draw_outline = false,
    draw_shades = true,
    use_xft = true,
    font = 'Noto Sans:size=10',
    gap_x = 5,
    gap_y = 60,
    minimum_height = 5,
        minimum_width = 5,
    net_avg_samples = 2,
    no_buffers = true,
    out_to_console = false,
    out_to_stderr = false,
    extra_newline = false,
    own_window = true,
    own_window_class = 'Conky',
    own_window_type = 'override',
    own_window_transparent = no,
    own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
    own_window_argb_visual = yes,
    own_window_argb_value = 0,
    stippled_borders = 0,
    update_interval = 1.0,
    uppercase = false,
    use_spacer = 'none',
    show_graph_scale = false,
    show_graph_range = false
}

conky.text = [[
${alignc}${color #3AA7DD}${font Openlogos:size=100}B${font}${color}
${alignc}$nodename
${alignc}$sysname $kernel on $machine
$hr
${font FontAwesome:size=14}${font}
 ${color grey}Uptime:$color $uptime
 ${color grey}Frequency (in GHz):$color $freq_g
 ${color grey}RAM Usage:$color $mem/$memmax ($memperc%)
 ${color grey}Swap Usage:$color $swap/$swapmax ($swapperc%)
 ${color grey}CPU Usage:$color $cpu%
 ${color grey}Processes:$color $processes  ${color grey}Running:$color $running_processes
$hr
${font FontAwesome:size=14} ${font}
 ${color grey}/ $color${fs_used /}/${fs_size /}
 ${color grey}/home $color${fs_used /home}/${fs_size /home}
 ${color grey}/gogn $color${fs_used /media/gogn}/${fs_size /media/gogn}
$hr
${font FontAwesome:size=14}${font}
 ${color red}${font FontAwesome:size=10}${font}:$color ${upspeed eth0} ${color grey}
 ${color green}${font FontAwesome:size=10}${font}:$color ${downspeed eth0}
$hr
${font FontAwesome:size=14}${font}
 ${color grey}${font FontAwesome:size=12}${font}+1=Terminal
 ${color grey}${font FontAwesome:size=12}${font}+2=Browser
 ${color grey}${font FontAwesome:size=12}${font}+3=Text Editor
 ${color grey}${font FontAwesome:size=12}${font}+4=IDE
 ${color grey}${font FontAwesome:size=12}${font}+8=Multimedia
 ${color grey}${font FontAwesome:size=12}${font}+9=Messaging
 ${color grey}${font FontAwesome:size=12}${font}+0=Music Streaming
$hr
]]
