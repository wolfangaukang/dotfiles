# Scripts

## Git

- **autosign_setup**: Sets Git's GPG signing.
- **memorize_credentials**: Useful to memorize credentials on a Git repository.
  - An alias can be set up with this.

# i3

- **lock-computer**: Executes `scrot` to take a blurred screenshot and `i3lock` to lock computer.
- **suspend-and-lock**: Does the same as `lock-computer`, but also relies on `encrypted-suspend` from LVM Suspend.

## LVM Suspend

- **encrypted-hibernate**: Allows hibernation when using LVM by shutting down to disk only.
- **encrypted-suspend**: Uses cryptsetup to suspend and resume.

## Non-NixOS

- **build-flake**: Builds flake according to the provided host.
  - An alias can be set up with this.
- **set_nix_and_build_flake**: Installs Nix into system, configure flakes and builds it.
  - Recommended only for setting up system for the first time.
- **set_zsh**: Sets ZSH as default shell.
  - Recommended only for setting up system for the first time.

## SSH

- **keygen_ed25519**: Generates SSH keys with the ed25519 signature system (more secure). Not recommended for generating keys to older servers.
  - An alias can be set up with this.

## Thelio

- **detect_pci_card**: Configures Pop!\_OS to recognize sound PCI card on Thelio. Assumes you are using PulseAudio.
  - Recommended only for setting up system for the first time.
- **set_hostname**: Sets hostname on Pop\_OS!.
  - Recommended only for setting up system for the first time.

## WIP

- **decrypt_annotations**: Decrypts annotations.
- **encrypt_annotations**: Encrypts annotations.
