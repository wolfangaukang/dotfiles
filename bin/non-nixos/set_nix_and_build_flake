#!/usr/bin/env bash
set -euo pipefail

function install_nix () {
	echo "-> Installing Nix..."
	[[ -d "/nix" ]] || sudo mkdir /nix
	curl -L https://nixos.org/nix/install | sh
	echo "-> Loading profile..."
	# shellcheck source=/dev/null
	. "${/home/$(whoami)/.nix-profile/etc/profile.d/nix.sh}"
	echo "-> Installing nixUnstable..."
	nix-env -f '<nixpkgs>' -iA nixUnstable
}

function set_up_nix () {
  if [ -d /nix ]; then
    echo "-> Are you sure you want to reinstall Nix (remove /nix/* and install)? [y/N]"
    read -r remove
    case $remove in
      [yY]) echo "-> Removing Nix directory..."
            rm -rf /nix/*
            install_nix;;
         *) echo "Alright, not removing it.";;
    esac
  else
    echo "-> /nix not found. Do you want to install it? [y/N]"
    read -r install
    case $install in
      [yY]) install_nix ;;
         *) echo "-> Not doing anything, exiting."
            exit 0;;
    esac
  fi
}

function build_flakes () {
  echo "-> Building Flakes from GitLab repo"
  export NIXPKGS_ALLOW_UNFREE=1
  if ! nix --experimental-features 'flakes nix-command' build "gitlab:wolfangaukang/dotfiles#$(hostname)" --impure --no-write-lock-file; then
      echo "-> ERROR. There were issues building the flake. Check the logs"
      exit 1
  else
      ./result/activate
      echo "-> SUCCESS. Flake activated."
  fi
}

echo "USE THIS SCRIPT ONLY TO SET EVERYTHING FOR THE FIRST TIME!!"
echo "Installs Nix on the current system and builds flake from WolfangAukang's Gitlab dotfiles. Do you want to advance? [y/N]"
read -r run
case $run in
  [yY]) set_up_nix
        build_flakes;;
     *) echo "Not doing anything. Exiting." && exit 0;;
esac
